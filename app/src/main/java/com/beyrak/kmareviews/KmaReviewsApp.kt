package com.beyrak.kmareviews

import android.app.Application
import com.beyrak.kmareviews.db.dao.FacultyDao
import com.beyrak.kmareviews.db.dao.ReviewDao
import com.beyrak.kmareviews.db.dao.TeacherDao
import com.beyrak.kmareviews.db.dao.UserDao
import com.beyrak.kmareviews.model.Faculty
import com.beyrak.kmareviews.model.Review
import com.beyrak.kmareviews.model.Teacher
import com.beyrak.kmareviews.model.User

open class KmaReviewsApp : Application() {

    companion object {
        lateinit var app: KmaReviewsApp
    }

    override fun onCreate() {
        super.onCreate()
        app = this
        populateDb()
    }

    private fun populateDb() {
        if (FacultyDao.db.getAllFacultiesCount() == 0) {
            FacultyDao.db.insertFaculties(
                listOf(
                    Faculty("Факультет Інформатики"),
                    Faculty("Факультет Гуманітарних Наук"),
                    Faculty("Факультет Економічних Наук"),
                    Faculty("Факультет Соціальних Наук і Технологій")
                )
            )

            TeacherDao.db.insertTeachers(
                listOf(
                    Teacher("Іван", "Іванов", "Факультет Інформатики"),
                    Teacher("Сергій", "Дуров", "Факультет Інформатики"),
                    Teacher("Ольга", "Мельник", "Факультет Гуманітарних Наук")
                )
            )

            UserDao.db.insertUsers(
                listOf(
                    User("test", "test@gmail.com", "Qwerty1", 2014, 2018)
                )
            )

            ReviewDao.db.insertReviews(
                listOf(
                    Review(1, "test", 5, "Чудовий викладач!")
                )
            )
        }
    }
}