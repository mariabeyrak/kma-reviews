package com.beyrak.kmareviews.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.beyrak.kmareviews.db.Db
import com.beyrak.kmareviews.model.Review

@Dao
abstract class ReviewDao {
    companion object {
        val db: ReviewDao by lazy { Db.instance.reviewDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertReview(review: Review)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertReviews(reviews: List<Review>) : List<Long>

    @Query("SELECT * FROM review WHERE teacherId = :teacherId")
    abstract fun getAllReviewsByTeacher(teacherId : Int): LiveData<List<Review>>

    @Query("SELECT COALESCE(AVG(rating), 0.0) FROM review WHERE teacherId = :teacherId")
    abstract fun getAverageRatingForTeacher(teacherId : Int): Double

    @Query("SELECT 1 FROM review WHERE reviewerUsername=:userName AND teacherId = :teacherId")
    abstract fun isUserLeftCommentForThisTeacher(userName: String, teacherId: Int): LiveData<Int>
}