package com.beyrak.kmareviews.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.beyrak.kmareviews.db.Db
import com.beyrak.kmareviews.model.Faculty

@Dao
abstract class FacultyDao {
    companion object {
        val db: FacultyDao by lazy { Db.instance.facultyDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertFaculties(faculties: List<Faculty>) : List<Long>

    @Query("SELECT COUNT (*) FROM faculty")
    abstract fun getAllFacultiesCount(): Int
}