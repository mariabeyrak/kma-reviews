package com.beyrak.kmareviews.db

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import com.beyrak.kmareviews.KmaReviewsApp.Companion.app
import com.beyrak.kmareviews.db.dao.FacultyDao
import com.beyrak.kmareviews.db.dao.ReviewDao
import com.beyrak.kmareviews.db.dao.TeacherDao
import com.beyrak.kmareviews.db.dao.UserDao
import com.beyrak.kmareviews.model.Faculty
import com.beyrak.kmareviews.model.Review
import com.beyrak.kmareviews.model.Teacher
import com.beyrak.kmareviews.model.User

@Database(entities = [Faculty::class, User::class, Teacher::class, Review::class],
    version = 1)
abstract class Db : RoomDatabase() {

    companion object {
        private const val DB_NAME = "kma_reviews_db"

        val instance: Db by lazy {
            Room.databaseBuilder(app, Db::class.java, DB_NAME)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .addMigrations(*MIGRATIONS)
                .build()
        }
    }

    abstract fun facultyDao(): FacultyDao

    abstract fun teacherDao(): TeacherDao

    abstract fun reviewDao(): ReviewDao

    abstract fun userDao(): UserDao

}

val MIGRATIONS = arrayOf<Migration>()