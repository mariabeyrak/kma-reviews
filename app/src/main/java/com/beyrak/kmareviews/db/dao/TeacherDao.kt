package com.beyrak.kmareviews.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.beyrak.kmareviews.db.Db
import com.beyrak.kmareviews.model.Teacher

@Dao
abstract class TeacherDao {
    companion object {
        val db: TeacherDao by lazy { Db.instance.teacherDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTeacher(teacher: Teacher)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertTeachers(teachers: List<Teacher>) : List<Long>

    @Query("SELECT * FROM teacher ORDER BY name, surname DESC")
    abstract fun getAllTeachers(): LiveData<List<Teacher>>

    @Query("SELECT * FROM teacher WHERE id=:id")
    abstract fun getTeacherById(id: Int): LiveData<Teacher>
}