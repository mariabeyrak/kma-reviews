package com.beyrak.kmareviews.db.dao

import android.provider.ContactsContract
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.beyrak.kmareviews.db.Db
import com.beyrak.kmareviews.model.User

@Dao
abstract class UserDao {
    companion object {
        val db: UserDao by lazy { Db.instance.userDao() }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertUser(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertUsers(users: List<User>) : List<Long>

    @Query("SELECT * FROM user WHERE email=:email")
    abstract fun getUserByEmail(email: String): User?

    @Query("SELECT * FROM user WHERE userName=:userName")
    abstract fun getUserByUsername(userName: String): User?

    @Query("SELECT 1 FROM user WHERE userName=:userName")
    abstract fun isUserExist(userName: String): Int

    @Query("SELECT 1 FROM user WHERE email=:email AND password=:password")
    abstract fun isUserExist(email: String, password: String): Int
}