package com.beyrak.kmareviews.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import com.beyrak.kmareviews.R
import com.beyrak.kmareviews.model.ui.TeacherWithRating
import com.beyrak.kmareviews.ui.base.BaseRecyclerAdapter

class TeachersWithRatingRecyclerAdapter : BaseRecyclerAdapter<TeacherWithRatingViewHolder, TeacherWithRating>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherWithRatingViewHolder {
            return TeacherWithRatingViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_teacher_with_rating, parent, false))
        }
    }