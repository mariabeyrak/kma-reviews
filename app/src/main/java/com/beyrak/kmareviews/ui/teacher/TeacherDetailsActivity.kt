package com.beyrak.kmareviews.ui.teacher

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyrak.kmareviews.R
import com.beyrak.kmareviews.databinding.ActivityMainBinding
import com.beyrak.kmareviews.databinding.ActivityTeacherDetailsBinding
import com.beyrak.kmareviews.model.ui.TeacherWithRating
import com.beyrak.kmareviews.ui.base.BaseVMActivity
import com.beyrak.kmareviews.ui.base.RecyclerClickListener
import com.beyrak.kmareviews.ui.main.MainViewModel
import com.beyrak.kmareviews.ui.main.TeachersWithRatingRecyclerAdapter
import com.beyrak.kmareviews.ui.review.AddReviewActivity
import kotlinx.android.synthetic.main.activity_teacher_details.*

class TeacherDetailsActivity : BaseVMActivity<TeacherDetailsViewModel>() {

    companion object {
        val KEY_TEACHER = "TEACHER.ID"

        fun getIntent(context: Context, teacherId: Int) = Intent(context, TeacherDetailsActivity::class.java)
            .putExtra(KEY_TEACHER, teacherId)
    }

    private val binding: ActivityTeacherDetailsBinding by lazy {
        DataBindingUtil.setContentView<ActivityTeacherDetailsBinding>(
            this,
            R.layout.activity_teacher_details
        )
    }
    private val adapter: ReviewsRecyclerAdapter by lazy { ReviewsRecyclerAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.teacherId.value = intent.getIntExtra(KEY_TEACHER, 0)

        binding.apply {
            viewModel = this@TeacherDetailsActivity.viewModel
            setLifecycleOwner(this@TeacherDetailsActivity)
            executePendingBindings()
        }

        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter

        viewModel.reviews.observe(this, Observer { reviews ->
            adapter.setModels(reviews)
        })

        viewModel.onAddComment.observe(this, Observer { teacherId: Int ->
            startActivity(AddReviewActivity.getIntent(this, teacherId))
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        when (item?.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun provideViewModel(): TeacherDetailsViewModel =
        ViewModelProviders.of(this).get(TeacherDetailsViewModel::class.java)
}

