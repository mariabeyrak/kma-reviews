package com.beyrak.kmareviews.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyrak.kmareviews.R
import com.beyrak.kmareviews.databinding.ActivityMainBinding
import com.beyrak.kmareviews.model.ui.TeacherWithRating
import com.beyrak.kmareviews.prefs.UserPrefs
import com.beyrak.kmareviews.ui.base.BaseVMActivity
import com.beyrak.kmareviews.ui.base.RecyclerClickListener
import com.beyrak.kmareviews.ui.teacher.TeacherDetailsActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseVMActivity<MainViewModel>(){

    companion object {
        fun getIntent(context: Context) = Intent(context, MainActivity::class.java)
    }

    private val binding: com.beyrak.kmareviews.databinding.ActivityMainBinding by lazy { DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main) }
    private val adapter: TeachersWithRatingRecyclerAdapter by lazy { TeachersWithRatingRecyclerAdapter() }

    private val clickListener = object : RecyclerClickListener<TeacherWithRating>() {
        override fun onClick(pos: Int, model: TeacherWithRating?) {
            model?.apply {
                startActivity(TeacherDetailsActivity.getIntent(this@MainActivity, model.id))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.apply {
            viewModel = this@MainActivity.viewModel
            setLifecycleOwner(this@MainActivity)
            executePendingBindings()
        }

        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)
        adapter.setClickListener(clickListener)
        recycler.adapter = adapter

        viewModel.teachersWithRating.observe(this, Observer { teachersWithRating ->
            adapter.setModels(teachersWithRating)
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        when (item?.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onBackPressed() {
        super.onBackPressed()
        UserPrefs.saveCurrentUser("")
    }

    override fun provideViewModel(): MainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
}
