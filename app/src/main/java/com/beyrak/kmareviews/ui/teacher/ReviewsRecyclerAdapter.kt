package com.beyrak.kmareviews.ui.teacher

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.beyrak.kmareviews.R
import com.beyrak.kmareviews.databinding.ItemReviewBinding
import com.beyrak.kmareviews.model.Review
import com.beyrak.kmareviews.ui.base.BaseRecyclerAdapter
import com.beyrak.kmareviews.ui.base.BaseViewHolder

class ReviewsRecyclerAdapter : BaseRecyclerAdapter<ReviewViewHolder, Review>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        return ReviewViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_review, parent, false))
    }
}

class ReviewViewHolder(itemView: View) : BaseViewHolder<Review>(itemView) {

    val binding: ItemReviewBinding? = DataBindingUtil.bind(itemView)

    override fun bind(model: Review) {
        binding?.apply {
            this.model = model
            formattedRating =  model.rating.toString()
            executePendingBindings()
        }
    }
}