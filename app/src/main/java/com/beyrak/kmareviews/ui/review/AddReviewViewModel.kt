package com.beyrak.kmareviews.ui.review

import androidx.lifecycle.MutableLiveData
import com.beyrak.kmareviews.db.dao.ReviewDao
import com.beyrak.kmareviews.model.Review
import com.beyrak.kmareviews.prefs.UserPrefs
import com.beyrak.kmareviews.ui.base.BaseViewModel

class AddReviewViewModel : BaseViewModel() {
    var teacherId: Int = 0
    val comment: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val mark: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    val onCommentAdded: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }

    fun onAddCommentClick() {
        ReviewDao.db.insertReview(
            Review(
                teacherId, UserPrefs.getCurrentUser(),
                mark.value?.toIntOrNull() ?: 0, comment.value
            )
        )
        onCommentAdded.postValue(Unit)
    }
}