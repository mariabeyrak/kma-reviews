package com.beyrak.kmareviews.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseVMActivity<T : BaseViewModel> : AppCompatActivity() {

    protected lateinit var viewModel: T

    abstract fun provideViewModel(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel()
    }
}