package com.beyrak.kmareviews.ui.review

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.beyrak.kmareviews.R
import com.beyrak.kmareviews.databinding.ActivityAddReviewBinding
import com.beyrak.kmareviews.ui.base.BaseVMActivity
import kotlinx.android.synthetic.main.activity_add_review.*

class AddReviewActivity : BaseVMActivity<AddReviewViewModel>() {

    companion object {
        val KEY_TEACHER = "TEACHER.ID"

        fun getIntent(context: Context, teacherId: Int) = Intent(context, AddReviewActivity::class.java)
            .putExtra(KEY_TEACHER, teacherId)
    }

    private val binding: ActivityAddReviewBinding by lazy {
        DataBindingUtil.setContentView<ActivityAddReviewBinding>(
            this,
            R.layout.activity_add_review
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.teacherId = intent.getIntExtra(KEY_TEACHER, 0)

        binding.apply {
            viewModel = this@AddReviewActivity.viewModel
            setLifecycleOwner(this@AddReviewActivity)
            executePendingBindings()
        }

        setSupportActionBar(toolbar)

        viewModel.onCommentAdded.observe(this, Observer {
            onBackPressed()
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean =
        when (item?.itemId) {
            android.R.id.home -> onBackPressed().let { true }
            else -> super.onOptionsItemSelected(item)
        }

    override fun provideViewModel(): AddReviewViewModel =
        ViewModelProviders.of(this).get(AddReviewViewModel::class.java)
}

