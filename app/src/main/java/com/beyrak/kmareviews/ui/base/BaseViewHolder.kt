package com.beyrak.kmareviews.ui.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

abstract class BaseViewHolder<M>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    protected var model: M? = null
    protected var clickListenerWeak: WeakReference<RecyclerClickListener<M>?> = WeakReference(null)

    fun bind(model: M, clickListener: WeakReference<RecyclerClickListener<M>?>) {
        clickListenerWeak = clickListener
        this.model = model
        bind(model)
    }

    abstract fun bind(model: M)

    open fun clearUp() {}
}