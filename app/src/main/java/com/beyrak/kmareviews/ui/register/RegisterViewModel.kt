package com.beyrak.kmareviews.ui.register

import androidx.lifecycle.MutableLiveData
import com.beyrak.kmareviews.db.dao.UserDao
import com.beyrak.kmareviews.model.User
import com.beyrak.kmareviews.prefs.UserPrefs
import com.beyrak.kmareviews.ui.base.BaseViewModel

class RegisterViewModel : BaseViewModel() {
    val username: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val email: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val password: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val startYear: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val endYear: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    val onRegister: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val onHasAccount: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val showAlert: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun onRegisterClick(){
        if (UserDao.db.isUserExist(username.value ?: "") == 1){
            showAlert.postValue("Username already exist")
        } else {
            UserDao.db.insertUser(User(username.value ?: "",
                email.value ?: "",
                password.value ?: "",
                startYear.value?.toIntOrNull() ?: 0,
                endYear.value?.toIntOrNull() ?: 0))
            UserPrefs.saveCurrentUser(username.value ?: "")
            onRegister.postValue(Unit)
        }
    }

    fun onHasAccountClick(){
        onHasAccount.postValue(Unit)
    }
}