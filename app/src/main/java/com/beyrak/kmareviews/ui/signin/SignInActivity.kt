package com.beyrak.kmareviews.ui.signin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.beyrak.kmareviews.R
import com.beyrak.kmareviews.databinding.ActivitySignInBinding
import com.beyrak.kmareviews.ui.base.BaseVMActivity
import com.beyrak.kmareviews.ui.main.MainActivity
import com.beyrak.kmareviews.ui.register.RegisterActivity

class SignInActivity : BaseVMActivity<SignInViewModel>(){

    companion object {
        fun getIntent(context: Context) = Intent(context, SignInActivity::class.java)
    }

    private val binding: ActivitySignInBinding by lazy { DataBindingUtil.setContentView<ActivitySignInBinding>(this, R.layout.activity_sign_in) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.checkIfRegistered()

        binding.apply {
            viewModel = this@SignInActivity.viewModel
            setLifecycleOwner(this@SignInActivity)
            executePendingBindings()
        }

        viewModel.onNoAccount.observe(this, Observer {
            startActivity(RegisterActivity.getIntent(this))
        })

        viewModel.onSignIn.observe(this, Observer {
            startActivity(MainActivity.getIntent(this))
            finish()
        })

        viewModel.showAlert.observe(this, Observer { text: String ->
            AlertDialog.Builder(this)
                .setMessage(text)
                .setPositiveButton("Ok", null)
                .show()
        })

    }

    override fun provideViewModel(): SignInViewModel = ViewModelProviders.of(this).get(SignInViewModel::class.java)
}
