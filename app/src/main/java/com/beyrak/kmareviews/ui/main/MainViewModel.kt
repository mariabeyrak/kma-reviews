package com.beyrak.kmareviews.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.beyrak.kmareviews.db.dao.ReviewDao
import com.beyrak.kmareviews.db.dao.TeacherDao
import com.beyrak.kmareviews.model.Teacher
import com.beyrak.kmareviews.model.ui.TeacherWithRating
import com.beyrak.kmareviews.ui.base.BaseViewModel

class MainViewModel : BaseViewModel() {
    val teachersWithRating: LiveData<MutableList<TeacherWithRating>> =
        Transformations.map(TeacherDao.db.getAllTeachers()) { teachers: List<Teacher> ->
            val teachersWithRating: MutableList<TeacherWithRating> = mutableListOf()
            teachers.forEach { teacher ->
                teachersWithRating.add(
                    TeacherWithRating(
                        teacher,
                        ReviewDao.db.getAverageRatingForTeacher(teacher.id)
                    )
                )
            }
            teachersWithRating
        }
}