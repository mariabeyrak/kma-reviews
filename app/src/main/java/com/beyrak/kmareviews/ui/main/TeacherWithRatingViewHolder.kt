package com.beyrak.kmareviews.ui.main

import android.view.View
import androidx.databinding.DataBindingUtil
import com.beyrak.kmareviews.databinding.ItemTeacherWithRatingBinding
import com.beyrak.kmareviews.model.ui.TeacherWithRating
import com.beyrak.kmareviews.ui.base.BaseViewHolder
import com.beyrak.kmareviews.ui.base.RecyclerClickListener

class TeacherWithRatingViewHolder(itemView: View) : BaseViewHolder<TeacherWithRating>(itemView) {

    val binding: ItemTeacherWithRatingBinding? = DataBindingUtil.bind(itemView)

    override fun bind(model: TeacherWithRating) {
        binding?.apply {
            this.model = model
            pos = adapterPosition
            formattedRating =  model.rating.toString()
            formattedName = "${model.name} ${model.surname}"
            clickListener = clickListenerWeak.get() as RecyclerClickListener<TeacherWithRating>
            executePendingBindings()
        }
    }
}