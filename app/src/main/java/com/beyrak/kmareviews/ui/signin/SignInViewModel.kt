package com.beyrak.kmareviews.ui.signin

import androidx.lifecycle.MutableLiveData
import com.beyrak.kmareviews.db.dao.UserDao
import com.beyrak.kmareviews.prefs.UserPrefs
import com.beyrak.kmareviews.ui.base.BaseViewModel

class SignInViewModel : BaseViewModel() {
    val email: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val password: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    val onSignIn: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val onNoAccount: MutableLiveData<Unit> by lazy { MutableLiveData<Unit>() }
    val showAlert: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun checkIfRegistered(){
        if (UserPrefs.getCurrentUser() != ""){
            onSignIn.postValue(Unit)
        }
    }

    fun onSignInClick(){
        if (UserDao.db.isUserExist(email.value ?: "", password.value ?: "") == 1){
            UserPrefs.saveCurrentUser(UserDao.db.getUserByEmail(email.value ?: "")?.username ?: "")
            onSignIn.postValue(Unit)
        } else {
            showAlert.postValue("Incorrect email or password")
        }
    }

    fun onNoAccountClick(){
        onNoAccount.postValue(Unit)
    }
}