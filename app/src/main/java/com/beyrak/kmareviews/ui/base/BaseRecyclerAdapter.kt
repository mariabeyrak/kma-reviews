package com.beyrak.kmareviews.ui.base

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import java.lang.ref.WeakReference

abstract class BaseRecyclerAdapter<VH : BaseViewHolder<M>, M : DiffComparable> : RecyclerView.Adapter<VH>() {

    protected val modelsList: MutableList<M> = ArrayList()
    private var clickListenerRef: WeakReference<RecyclerClickListener<M>?> = WeakReference(null)

    fun setClickListener(clickListener: RecyclerClickListener<M>?) {
        if (clickListenerRef.get() == clickListener) return
        clickListenerRef = WeakReference(clickListener)
    }

    fun setModels(models: List<M>) {
        val diffCallback: DiffCallback<M> = DiffCallback(this.modelsList, models)
        val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(diffCallback)

        this.modelsList.clear()
        this.modelsList.addAll(models)
        diffResult.dispatchUpdatesTo(this)
    }

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH

    open override fun onBindViewHolder(holder: VH, position: Int) {
        bindHolder(holder, position, modelsList[position])
    }

    open fun bindHolder(holder: VH, position: Int, model: M) {
        holder.bind(model, clickListenerRef)
    }

    open override fun getItemCount(): Int = modelsList.size

    override fun onViewRecycled(holder: VH) {
        super.onViewRecycled(holder)
        holder.clearUp()
    }
}

private class DiffCallback<T : DiffComparable>(val oldList: List<T>, val newList: List<T>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].areItemsTheSame(newList[newItemPosition])

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].areContentsTheSame(newList[newItemPosition])

}