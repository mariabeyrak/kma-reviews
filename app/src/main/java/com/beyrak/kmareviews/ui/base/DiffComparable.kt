package com.beyrak.kmareviews.ui.base

interface DiffComparable {
    fun areItemsTheSame(item: Any): Boolean

    fun areContentsTheSame(item: Any): Boolean
}