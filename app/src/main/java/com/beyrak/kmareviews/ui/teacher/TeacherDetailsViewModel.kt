package com.beyrak.kmareviews.ui.teacher

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.beyrak.kmareviews.db.dao.ReviewDao
import com.beyrak.kmareviews.db.dao.TeacherDao
import com.beyrak.kmareviews.model.Review
import com.beyrak.kmareviews.model.Teacher
import com.beyrak.kmareviews.prefs.UserPrefs
import com.beyrak.kmareviews.ui.base.BaseViewModel

class TeacherDetailsViewModel : BaseViewModel() {
    val teacherId: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val teacher: LiveData<Teacher> = Transformations.switchMap(teacherId) { teacherId: Int ->
        TeacherDao.db.getTeacherById(teacherId)
    }

    val formattedName: LiveData<String> = Transformations.map(teacher) { teacher: Teacher ->
        "${teacher.name} ${teacher.surname}"
    }

    val reviews: LiveData<List<Review>> = Transformations.switchMap(teacherId) { teacherId: Int ->
        ReviewDao.db.getAllReviewsByTeacher(teacherId)
    }

    val isUserAlreadyAddedComment: LiveData<Boolean> = Transformations.switchMap(teacherId) { teacherId: Int ->
        Transformations.map(
            ReviewDao.db.isUserLeftCommentForThisTeacher(
                UserPrefs.getCurrentUser(),
                teacherId
            )
        ) { isUserLeftCommentForThisTeacher: Int? ->
            isUserLeftCommentForThisTeacher ?: 0 == 1
        }
    }

    val onAddComment: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }

    fun onAddCommentClick() {
        onAddComment.postValue(teacherId.value ?: return)
    }
}