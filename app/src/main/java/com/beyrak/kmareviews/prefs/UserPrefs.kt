package com.beyrak.kmareviews.prefs

import android.content.Context
import android.content.SharedPreferences
import com.beyrak.kmareviews.KmaReviewsApp

private const val KEY_CURRENT_USERNAME = "key.username"

object UserPrefs {
    private val prefs: SharedPreferences by lazy { KmaReviewsApp.app.getSharedPreferences("userPrefs", Context.MODE_PRIVATE) }

    fun saveCurrentUser(currentUserName: String) = prefs.edit().putString(KEY_CURRENT_USERNAME, currentUserName).apply()
    fun getCurrentUser(): String = prefs.getString(KEY_CURRENT_USERNAME, "")
}