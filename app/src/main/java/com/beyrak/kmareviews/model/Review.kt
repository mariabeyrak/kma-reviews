package com.beyrak.kmareviews.model

import androidx.room.Entity
import com.beyrak.kmareviews.ui.base.DiffComparable

@Entity(tableName = "review", primaryKeys = ["teacherId", "reviewerUsername"])
data class Review(
    val teacherId: Int,
    val reviewerUsername: String,
    val rating: Int,
    val text: String? = null
) : DiffComparable {
    override fun areItemsTheSame(item: Any): Boolean =
        (item as? Review)?.let { it.teacherId == teacherId && it.reviewerUsername == reviewerUsername } ?: false

    override fun areContentsTheSame(item: Any): Boolean =
        (item as? Review)?.let {
            it.teacherId == teacherId && it.reviewerUsername == reviewerUsername
                    && it.rating == rating && it.text == text
        } ?: false
}