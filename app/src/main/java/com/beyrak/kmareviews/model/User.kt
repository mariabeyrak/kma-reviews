package com.beyrak.kmareviews.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @PrimaryKey val username: String,
    val email: String,
    val password: String,
    val startYear: Int,
    val endYear: Int
)