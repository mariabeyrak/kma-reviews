package com.beyrak.kmareviews.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "faculty")
data class Faculty(
    @PrimaryKey val title: String
)