package com.beyrak.kmareviews.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "teacher")
data class Teacher(
    val name: String,
    val surname: String,
    val faculty: String,
    val description: String? = null
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}