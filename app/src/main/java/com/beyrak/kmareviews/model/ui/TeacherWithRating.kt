package com.beyrak.kmareviews.model.ui

import com.beyrak.kmareviews.model.Teacher
import com.beyrak.kmareviews.ui.base.DiffComparable

data class TeacherWithRating(
    val id: Int,
    val name: String,
    val surname: String,
    val faculty: String,
    val rating: Double,
    val description: String? = null
) : DiffComparable {
    constructor(teacher: Teacher, rating: Double) : this(
        teacher.id, teacher.name, teacher.surname, teacher.faculty,
        rating, teacher.description
    )

    override fun areItemsTheSame(item: Any): Boolean =
        (item as? TeacherWithRating)?.let { it.id == id } ?: false

    override fun areContentsTheSame(item: Any): Boolean =
        (item as? TeacherWithRating)?.let {
            it.id == id && it.name == name && it.surname == surname
                    && it.faculty == faculty && it.rating == rating && it.description == description
        } ?: false
}


